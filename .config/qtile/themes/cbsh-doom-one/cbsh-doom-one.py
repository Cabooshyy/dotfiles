from libqtile.layout.floating import Floating
from libqtile.config import Match, Rule

class Colors(object):

    black           = ['#21242b', '#21242b']
    grey            = ['#3f444a', '#3f444a']
    white           = ['#DFDFDF', '#DFDFDF']
    red             = ['#ff6c6b', '#ff6c6b']
    brightred       = ['#da8548', '#da8548']
    green           = ['#98be65', '#98be65']
    brightgreen     = ['#4db5bd', '#4db5bd']
    blue            = ['#51afef', '#51afef']
    darkblue        = ['#2257A0', '#2257A0']
    magenta         = ['#c678dd', '#c678dd']
    brightmagenta   = ['#a9a1e1', '#a9a1e1']
    cyan            = ['#5699AF', '#5699AF']
    brightcyan      = ['#46D9FF', '#46D9FF']


class Fonts(object):
    base = "SauceCodePro Nerd Font"
    bold = "SauceCodePro Nerd Font Bold"

class Wallpaper(object):
    wallpaper = "~/.config/qtile/themes/cbsh-doom-one/cbsh-doom-one.png"
