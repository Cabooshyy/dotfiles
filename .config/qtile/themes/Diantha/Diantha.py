#!/usr/bin/env python3
from libqtile.layout.floating import Floating
from libqtile.config import Match, Rule

class Colors(object):

    black           = ['#351e28', '#351e28']
    grey            = ['#5F5F64', '#5F5F64']
    white           = ['#ffffff', '#ffffff']
    red             = ['#5c7347', '#5c7347']
    brightred       = ['#a6d181', '#a6d181']
    green           = ['#a38089', '#a38089']
    brightgreen     = ['#ed85ae', '#ed85ae']
    blue            = ['#225073', '#225073']
    darkblue        = ['#4cb1ff', '#4cb1ff']
    magenta         = ['#94536d', '#94536d']
    brightmagenta   = ['#f5c1ce', '#f5c1ce']
    cyan            = ['#e5e2a3', '#e5e2a3']
    brightcyan      = ['#faf6b2', '#faf6b2']


class Fonts(object):
    base = "SauceCodePro Nerd Font"
    bold = "SauceCodePro Nerd Font Bold"

class Wallpaper(object):
    wallpaper = "~/.config/qtile/themes/Diantha/BG.png"
