#!/usr/bin/env python3
from libqtile.layout.floating import Floating
from libqtile.config import Match, Rule

class Colors(object):

    black           = ['#23345f', '#23345f']
    grey            = ['#5F5F64', '#5F5F64']
    white           = ['#ffffff', '#ffffff']
    red             = ['#4f77a2', '#4f77a2']
    brightred       = ['#9fd2f1', '#9fd2f1']
    green           = ['#8a3340', '#8a3340']
    brightgreen     = ['#d65064', '#d65064']
    blue            = ['#4363b5', '#4363b5']
    darkblue        = ['#2e447d', '#2e447d']
    magenta         = ['#d9b527', '#d9b527']
    brightmagenta   = ['#fbd23e', '#fbd23e']
    cyan            = ['#88b6d2', '#88adbe']
    brightcyan      = ['#d0dcf2', '#d0dcf2']


class Fonts(object):
    base = "SauceCodePro Nerd Font"
    bold = "SauceCodePro Nerd Font Bold"

class Wallpaper(object):
    wallpaper = "~/.config/qtile/themes/Skyla/BG.png"
