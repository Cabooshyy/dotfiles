#!/usr/bin/env python3
from libqtile.layout.floating import Floating
from libqtile.config import Match, Rule

class Colors(object):

    black           = ['#303446', '#303446']
    grey            = ['#626880', '#626880']
    white           = ['#c6d0f5', '#c6d0f5']
    red             = ['#e78284', '#e78284']
    brightred       = ['#ea999c', '#ea999c']
    green           = ['#51576d', '#51576d']
    brightgreen     = ['#737994', '#737994']
    blue            = ['#85c1dc', '#85c1dc']
    darkblue        = ['#8caaee', '#8caaee']
    magenta         = ['#ca9ee6', '#ca9ee6']
    brightmagenta   = ['#f4b8e4', '#f4b8e4']
    cyan            = ['#a5adce', '#a5adce']
    brightcyan      = ['#b5bfe2', '#b5bfe2']


class Fonts(object):
    base = "SauceCodePro Nerd Font"
    bold = "SauceCodePro Nerd Font Bold"

class Wallpaper(object):
    wallpaper = "~/.config/qtile/themes/cbsh-doom-vibrant/cbsh-doom-vibrant.png"
