#!/usr/bin/env python3
from libqtile.layout.floating import Floating
from libqtile.config import Match, Rule

class Colors(object):

    black           = ['#0A1533', '#0A1533']
    grey            = ['#5F5F64', '#5F5F64']
    white           = ['#ffffff', '#ffffff']
    red             = ['#87495a', '#87495a']
    brightred       = ['#C2124F', '#C2124F']
    green           = ['#165099', '#165099']
    brightgreen     = ['#38A1F0', '#38A1F0']
    blue            = ['#014AA7', '#014AA7']
    darkblue        = ['#0D1F73', '#0D1F73']
    magenta         = ['#2B8FF0', '#2B8FF0']
    brightmagenta   = ['#2EA3EE', '#2EA3EE']
    cyan            = ['#3AD0EE', '#3AD0EE']
    brightcyan      = ['#7CEEF6', '#7CEEF6']


class Fonts(object):
    base = "SauceCodePro Nerd Font"
    bold = "SauceCodePro Nerd Font Bold"

class Wallpaper(object):
    wallpaper = "~/.config/qtile/themes/cbsh-black-hole/cbsh-black-hole.png"
